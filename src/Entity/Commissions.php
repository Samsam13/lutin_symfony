<?php

namespace App\Entity;

use App\Repository\CommissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommissionRepository::class)
 */
class Commissions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */

    private $id;
/**
 * @ORM\Column(type="string", length=255)
 */
private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min={3}, max={255})
     */
    private $nickname;

    /**
     * @ORM\Column(type="date")
     */
    private $installation_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $frequency;

    /**
     * @ORM\Column(type="string")
     * @Assert\Positive()
     */
    private $president;

        /**
         * @ORM\Column(type="string")
         */
    private $vice_president;

    /**
     * @ORM\Column (type="string")
     */
    private $url_img;

        /**
         * @ORM\Column(type="string")
         */
    private $url_agreement;

        /**
         * @ORM\Column(type="string")
         */
    private $url_invitation_template;

            /**
             * @ORM\Column(type="string")
             * @Assert\Url()
             */
    private $url_minutes_template;

                /**
                 * @ORM\Column(type="text")
                 */
    private $alternation_rules;

                    /**
                     * @ORM\Column(type="text")
                     */
    private $composition_social_section;


                    /**
                     * @ORM\Column(type="text")
                     */
    private $composition_professionnal_section;
    //@ORM\ManyToMany(targetEntity="App\Entity\Members", mappedBy="commissions")

    /**
     *
     */
    //private $members;
    /*public function __construct(){
        $this->members = new ArrayCollection();
    }
    */

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param mixed $nickname
     */
    public function setNickname($nickname): void
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getInstallationDate()
    {
        return $this->installation_date;
    }

    /**
     * @param mixed $installation_date
     */
    public function setInstallationDate($installation_date): void
    {
        $this->installation_date = $installation_date;
    }

    /**
     * @return mixed
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param mixed $frequency
     */
    public function setFrequency($frequency): void
    {
        $this->frequency = $frequency;
    }

    /**
     * @return mixed
     */
    public function getPresident()
    {
        return $this->president;
    }

    /**
     * @param mixed $president
     */
    public function setPresident($president): void
    {
        $this->president = $president;
    }

    /**
     * @return mixed
     */
    public function getVicePresident()
    {
        return $this->vice_president;
    }

    /**
     * @param mixed $vice_president
     */
    public function setVicePresident($vice_president): void
    {
        $this->vice_president = $vice_president;
    }

    /**
     * @return mixed
     */
    public function getUrlAgreement()
    {
        return $this->url_agreement;
    }
    /**
     * @param mixed $url_agreement
     */
    public function setUrlAgreement($url_agreement): void
    {
        $this->url_agreement = $url_agreement;
    }

    /**
     * @return mixed
     */
    public function getUrlImg()
    {
        return $this->url_img;
    }

    /**
     * @param mixed $url_img
     */
    public function setUrlImg($url_img): void
    {
        $this->url_img = $url_img;
    }

    /**
     * @return mixed
     */
    public function getUrlInvitationTemplate()
    {
        return $this->url_invitation_template;
    }

    /**
     * @param mixed $url_invitation_template
     */
    public function setUrlInvitationTemplate($url_invitation_template): void
    {
        $this->url_invitation_template = $url_invitation_template;
    }

    /**
     * @return mixed
     */
    public function getAlternationRules()
    {
        return $this->alternation_rules;
    }

    /**
     * @param mixed $alternation_rules
     */
    public function setAlternationRules($alternation_rules): void
    {
        $this->alternation_rules = $alternation_rules;
    }

    /**
     * @return mixed
     */
    public function getCompositionProfessionnalSection()
    {
        return $this->composition_professionnal_section;
    }

    /**
     * @param mixed $composition_professionnal_section
     */
    public function setCompositionProfessionnalSection($composition_professionnal_section): void
    {
        $this->composition_professionnal_section = $composition_professionnal_section;
    }

    /**
     * @return mixed
     */
    public function getCompositionSocialSection()
    {
        return $this->composition_social_section;
    }

    /**
     * @param mixed $composition_social_section
     */
    public function setCompositionSocialSection($composition_social_section): void
    {
        $this->composition_social_section = $composition_social_section;
    }

    /**
     * @return mixed
     */
    public function getUrlMinutesTemplate()
    {
        return $this->url_minutes_template;
    }

    /**
     * @param mixed $url_minutes_template
     */
    public function setUrlMinutesTemplate($url_minutes_template): void
    {
        $this->url_minutes_template = $url_minutes_template;
    }
}
