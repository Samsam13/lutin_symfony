<?php

namespace App\DataFixtures;

use App\Entity\Commission;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i<50; $i++) {
            $commission = new Commission();
            $commission
                ->setName('Commission' .$i);

        }
        // $product = new Product();

        $manager->persist($commission);

        $manager->flush();
    }
}
