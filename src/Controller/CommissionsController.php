<?php

namespace App\Controller;

use App\Repository\CommissionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommissionsController extends AbstractController
{
    /**
     * @Route("/home", name="homepage")
     */

    public function getCommissions(CommissionRepository $repository):Response
    {
       return $this->render('home.html.twig', [
            'commissions' => $repository->findAll()
        ]);

    }
}