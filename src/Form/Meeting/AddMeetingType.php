<?php

namespace App\Form\Meeting;

use App\Form\AddMeeting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddMeetingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $builder
            ->add('commission_id', IntegerType::class)
            ->add('date', DateType::class)
            ->add('quorum', null)//Type Booléen
            ->add('agenda')
            ->add('url_minutes', null, [
                'help' => 'le lien du PV de la commission'
            ] )
            ->add('provisional_preparation_date', DateType::class)
            ->add('real_preparation_date', DateType::class)
            ->add('invitation_date', DateType::class)
            ->add('status_minutes')
            ->add('assistant')
            ->add('president')
            ;
    }
    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefault([
            'data_class' => AddMeeting::class,
        ]);
    }
}
