<?php

namespace Container1YldVfr;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getUpdateMeetingTypeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Form\Meeting\UpdateMeetingType' shared autowired service.
     *
     * @return \App\Form\Meeting\UpdateMeetingType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/Meeting/UpdateMeetingType.php';

        return $container->privates['App\\Form\\Meeting\\UpdateMeetingType'] = new \App\Form\Meeting\UpdateMeetingType();
    }
}
