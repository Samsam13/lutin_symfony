<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/_header.html.twig */
class __TwigTemplate_c55110ad525be24d380ea519e151277d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/_header.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/_header.html.twig"));

        // line 1
        echo "<header>
    <div class=\"row bg-white\">
        <div class=\"col-3\">
            <a href=\"http://localhost:8888/lutin/\">
                <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo_cpam_indre.png"), "html", null, true);
        echo "\" class=\"float-start\" alt=\"logo de l'assurance maladie de l'Indre\">
            </a>

        </div>
        <div class=\"col-9 text-center p-2\">
            <h1>Lutin</h1>
            <h2>Gestion des commissions paritaires des professionnels de santé</h2>
        </div>

    </div>

    <nav class=\"row bg-light py-3 mb-4\">
        <div class=\"col-3 text-center\"><a href=\"index.php?action=members\" class=\"text-reset text-decoration-none\">Les
                membres des commissions</a></div>
        <div class=\"col-3 border-end border-start border-dark text-center\"><a href=\"index.php\"
                                                                              class=\"text-reset text-decoration-none\">Les
                commissions</a></div>
        ";
        // line 29
        echo "
        <div class=\"col-3 border-end border-start border-dark text-center\"><a href=\"index.php?action=users\"
                                                                              class=\"text-reset text-decoration-none\">Les
                utilisateurs</a></div>


    </nav>
</header>";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    /**
     * @codeCoverageIgnore
     */
    public function getTemplateName()
    {
        return "partials/_header.html.twig";
    }

    /**
     * @codeCoverageIgnore
     */
    public function isTraitable()
    {
        return false;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDebugInfo()
    {
        return array (  69 => 29,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header>
    <div class=\"row bg-white\">
        <div class=\"col-3\">
            <a href=\"http://localhost:8888/lutin/\">
                <img src=\"{{ asset('images/logo_cpam_indre.png') }}\" class=\"float-start\" alt=\"logo de l'assurance maladie de l'Indre\">
            </a>

        </div>
        <div class=\"col-9 text-center p-2\">
            <h1>Lutin</h1>
            <h2>Gestion des commissions paritaires des professionnels de santé</h2>
        </div>

    </div>

    <nav class=\"row bg-light py-3 mb-4\">
        <div class=\"col-3 text-center\"><a href=\"index.php?action=members\" class=\"text-reset text-decoration-none\">Les
                membres des commissions</a></div>
        <div class=\"col-3 border-end border-start border-dark text-center\"><a href=\"index.php\"
                                                                              class=\"text-reset text-decoration-none\">Les
                commissions</a></div>
        {#
        if (!empty(\$_SESSION['email'])) {
        echo '<div class=\"col-3 text-center\"><a href=\"index.php?action=logout\" class=\"text-reset text-decoration-none\">Se déconnecter :' . \$_SESSION['profile'] .'</a></div>';
        } else {
        echo '<div class=\"col-3 text-center\"><a href=\"index.php?action=login\" class=\"text-reset text-decoration-none\">Connexion</a></div>';
        }
        #}

        <div class=\"col-3 border-end border-start border-dark text-center\"><a href=\"index.php?action=users\"
                                                                              class=\"text-reset text-decoration-none\">Les
                utilisateurs</a></div>


    </nav>
</header>", "partials/_header.html.twig", "/Users/utilisateur/Documents/OC-local/PHP_symfony/lutin_symfony/lutin/templates/partials/_header.html.twig");
    }
}
