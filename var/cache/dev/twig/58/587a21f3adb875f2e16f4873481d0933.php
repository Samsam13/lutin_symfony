<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/_footer.html.twig */
class __TwigTemplate_4cc9dfef3d48aaa84ab6ec7e8d2f6956 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/_footer.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/_footer.html.twig"));

        // line 1
        echo "<footer class=\"bg-light text-center text-lg-start mt-5\">
    <div class=\"row\">
        <div class=\"col-4 text-center p-3\">
            © 2021 Copyright : <a class=\"fw-bold\" href=\"https://ameli.fr/\">CPAM de l'Indre</a>
        </div>
        <div class=\"col-4 text-center p-3\">
            <a href=\"#\">Mentions légales</a>
        </div>
        <div class=\"col-4 text-center p-3\">
            <a href=\"mailto:sami.gafsi@assurance-maladie.fr\">Contacter l'administrateur</a>
        </div>
    </div>


</footer>";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    /**
     * @codeCoverageIgnore
     */
    public function getTemplateName()
    {
        return "partials/_footer.html.twig";
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer class=\"bg-light text-center text-lg-start mt-5\">
    <div class=\"row\">
        <div class=\"col-4 text-center p-3\">
            © 2021 Copyright : <a class=\"fw-bold\" href=\"https://ameli.fr/\">CPAM de l'Indre</a>
        </div>
        <div class=\"col-4 text-center p-3\">
            <a href=\"#\">Mentions légales</a>
        </div>
        <div class=\"col-4 text-center p-3\">
            <a href=\"mailto:sami.gafsi@assurance-maladie.fr\">Contacter l'administrateur</a>
        </div>
    </div>


</footer>", "partials/_footer.html.twig", "/Users/utilisateur/Documents/OC-local/PHP_symfony/lutin_symfony/lutin/templates/partials/_footer.html.twig");
    }
}
